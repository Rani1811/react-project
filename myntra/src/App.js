import React from "react";
import { Routes, Route } from "react-router-dom";
import Navbar from "./Components/Navbar";
import Home from "./Components/Home";
import RegistrationForm from "./Components/RegistrationForm";
import Mobiles from "./Subcomponents/Mobiles";
import Electronics from "./Subcomponents/Electronics";
import HomeFurniture from "./Subcomponents/Home&Furniture";
import Fashion from "./Subcomponents/Fashion";

import Appliance from "./Subcomponents/Appliance";
import Login from  "./Subcomponents/Login";
import CartPage from "./Subcomponents/cartPage";


function App() {
  

  return (
    <>
      <Navbar />
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/Mobiles" element={<Mobiles />} />
        <Route path="/Electronics" element={<Electronics />} />
        <Route path="/Home&Furniture" element={<HomeFurniture />} />
        <Route path="/Fashion" element={<Fashion />} />
        <Route path="/Appliance" element={<Appliance />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/Cart" element={<CartPage />} />
        <Route path="/RegistrationForm" element={<RegistrationForm/>} />
      </Routes>
    </>
  );
}

export default App;
