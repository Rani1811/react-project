
import React from "react";

import { useSelector, useDispatch } from "react-redux";
import { addToCart } from "../Feature/ApplianceSlice";

import "../ComponentsCss/Card.css";


export default function App() {

    const appliances = useSelector((state) => state.ApplianceCart.items); //access the items from list

    const dispatch = useDispatch(); //dispatch action to store

  return (
    <div style={{display:"flex", flexWrap: "wrap", justifyContent: "space-between"}}>
      {appliances.map((appliance) => (
        <div key={appliance.id} >
          <div className="card-bodys" key={appliance.id} style={{height: "520px" }}>
            <img
              src={appliance.image}
              style={{ width: 180, height: 250 }}
              alt={appliance.name}
            />
            <h6 className="card-datas" style={{ fontWeight: "bold" }}>
              {appliance.name}
            </h6>
            <h6 className="card-datas">
              ₹{appliance.price} , {appliance.color}
            </h6>
            <h6 className="card-datas">{appliance.Description}</h6>
              <button className="addCarts" onClick={ () => dispatch(addToCart(appliance), alert("Product Add to cart"))}>Add to Cart</button>
          </div>
        </div>
      ))}
    </div>
  );
};

