const ApplianceList = [
  {
    id: 1,
    name: "Washing Machine ",
    price: 25000,
    color: "Silver",
    image:
      "https://rukminim1.flixcart.com/image/416/416/xif0q/washing-machine-new/y/v/5/-original-imagqjcnypg9czh7.jpeg?q=70",
    Description:
      "LG 7 kg with Smart Inverter Fully Automatic Top Load Washing Machine",
    quantity: 1,
  },
  {
    id: 2,
    name: "Refrigerator",
    price: 30000,
    color: "Elegant Inox, RT28A3453S8/HL",
    image:
      "https://rukminim1.flixcart.com/image/416/416/xif0q/refrigerator-new/u/v/n/-original-imagzxmhwtxf9jcg.jpeg?q=70",
    Description: "253 L Frost Free Double Door 3 Star Refrigerator ",
    quantity: 1,
  },

  {
    id: 3,
    name: "Oven Toaster Gril ",
    price: 5000,
    color: "Black",
    image:
      "https://rukminim1.flixcart.com/image/416/416/xif0q/otg-new/f/n/u/1500-sa-5033-digi-glen-33-original-imagkrwb8qezwg3n.jpeg?q=70",
    Description: "Full Back Convection, Motorized Rotisserie, 1500W Power",
    quantity: 1,
  },

  {
    id: 4,
    name: "Mixer Grinder  ",
    price: 2000,
    color: "Blue",
    image:
      "https://rukminim1.flixcart.com/image/416/416/xif0q/mixer-grinder-juicer/i/0/m/-original-imaghy69gbrjwkvz.jpeg?q=70",
    Description: "500 W : Higher the Wattage, tougher the Juicing/Grinding",
    quantity: 1,
  },

  {
    id: 5,
    name: "Iron",
    price: 1000,
    color: "Black",
    image:
      "https://rukminim1.flixcart.com/image/416/416/kerfl3k0pkrrdj/iron/r/s/p/usha-ei-3302-original-imafvhnkwnfxdzh2.jpeg?q=70",
    Description: "EI 3302 1100 W Dry Iron",
    quantity: 1,
  },

  {
    id: 6,
    name: " Air Cooler",
    price: 7000,
    color: "White",
    image:
      "https://rukminim1.flixcart.com/image/416/416/xif0q/air-cooler/g/5/m/-original-imagnhm7yahtsdah.jpeg?q=70",
    Description: "Tank Capacity: 24 L ,18 ft Air Throw",
    quantity: 1,
  },
];

export default ApplianceList;
