const FurnitureList =[
    {
        id:1 ,
       name :'Box Bed',
       price: 15000,
       color:'Brown',
       image:'https://rukminim1.flixcart.com/image/416/416/xif0q/bed/j/y/5/-original-imagpprf9wppvhuy.jpeg?q=70',
       Description:'Flipkart Perfect Homes Waltz Engineered Wood King Box Bed',
    
    },
    {
        id:2 ,
       name :'Sofa',
       price: 11000,
       color:'Grey',
       image:'https://rukminim1.flixcart.com/image/416/416/xif0q/recliner/x/c/2/76-2-1-seater-grey-99-06-velvet-bls-recliner-grey-bharat-original-imagmwddn6yhcjnq.jpeg?q=70',
       Description:'Lifestyle Austin Fabric Manual Recliner',
    
    },
    {
        id:3 ,
       name :'Painting',
       price: 500,
       color:'Brown',
       image:'https://rukminim1.flixcart.com/image/416/416/jyrl4sw0/painting/3/x/g/pnf-5839-12x18frame-poster-n-frames-original-imafgxhnhzhuftcw.jpeg?q=70',
       Description:'Poster N Frames UV Textured Decorative Art Print of Buddha ',
    
    },
    {
        id:4 ,
       name :'Darkening Door Curtain',
       price:500 ,
       color:'Purle',
       image:'https://rukminim1.flixcart.com/image/416/416/l0igvww0/curtain/i/c/u/cvc4re16-tre1pn16-door-eyelet-gratify-original-imagca7f3fpnhwpd.jpeg?q=70',
       Description:'Vidhi-Tree-Punching-7-FT-2-Piece-Purple',
    
    },
    {
        id: 5,
       name :'Analog Wall Watch',
       price: 600,
       color:'Brown With Glass, Standard',
       image:'https://rukminim1.flixcart.com/image/416/416/kaijgy80/wall-clock/r/w/g/analoge-33-cm-x-33-cm-wall-clock-st-58745638-analog-ajanta-original-imafs2z2te2pchjz.jpeg?q=70',
       Description:'Size: 33 cm x 33 cm ,Mechanism: wall mounted ',
    
    },
    {
        id:6 ,
       name :'Wall Lamp With Bulb',
       price:300 ,
       color:'White & Gold ',
       image:'https://rukminim1.flixcart.com/image/416/416/k2tc1ow0pkrrdj/wall-lamp/g/n/3/goj-22-114-white-shri-girraj-ji-original-imaeeyvs2ja338za.jpeg?q=70',
       Description:'Material: ["Glass","Metal"] ,Surface Mounted ,Bulb Used: ["LED"]',
    
    },

]

export default FurnitureList ;