
    const List =[
      {
        "id": 1,
        "name": "Washing Machine ",
        "price": 25000,
        "color": "Silver",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/washing-machine-new/y/v/5/-original-imagqjcnypg9czh7.jpeg?q=70",
        "Description": "LG 7 kg with Smart Inverter Fully Automatic Top Load Washing Machine",
        "quantity": 1
      },
      {
        "id": 2,
        "name": "Refrigerator",
        "price": 30000,
        "color": "Elegant Inox, RT28A3453S8/HL",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/refrigerator-new/u/v/n/-original-imagzxmhwtxf9jcg.jpeg?q=70",
        "Description": "253 L Frost Free Double Door 3 Star Refrigerator ",
        "quantity": 1
      },
  
      {
        "id": 3,
        "name": "Oven Toaster Gril ",
        "price": 5000,
        "color": "Black",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/otg-new/f/n/u/1500-sa-5033-digi-glen-33-original-imagkrwb8qezwg3n.jpeg?q=70",
        "Description": "Full Back Convection, Motorized Rotisserie, 1500W Power",
        "quantity": 1
      },
  
      {
        "id": 4,
        "name": "Mixer Grinder  ",
        "price": 2000,
        "color": "Blue",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mixer-grinder-juicer/i/0/m/-original-imaghy69gbrjwkvz.jpeg?q=70",
        "Description": "500 W : Higher the Wattage, tougher the Juicing/Grinding",
        "quantity": 1
      },
  
      {
        "id": 5,
        "name": "Iron",
        "price": 1000,
        "color": "Black",
        "image": "https://rukminim1.flixcart.com/image/416/416/kerfl3k0pkrrdj/iron/r/s/p/usha-ei-3302-original-imafvhnkwnfxdzh2.jpeg?q=70",
        "Description": "EI 3302 1100 W Dry Iron",
        "quantity": 1
      },
  
      {
        "id": 6,
        "name": " Air Cooler",
        "price": 7000,
        "color": "White",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/air-cooler/g/5/m/-original-imagnhm7yahtsdah.jpeg?q=70",
        "Description": "Tank Capacity: 24 L ,18 ft Air Throw",
        "quantity": 1
      },
  
      {
        "id": 1,
        "name": "APPLE iPhone 14",
        "price": 70000,
        "color": "Midnight",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/9/e/e/-original-imaghx9q5rvcdghy.jpeg?q=70",
        "Description": "128 GB ROM | 15.49 cm (6.1 inch) Super Retina XDR Display",
        "quantity": 1
      },
      {
        "id": 2,
        "name": "APPLE iPhone 14",
        "price": 70000,
        "color": "Purple",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/b/u/f/-original-imaghxa5hvapbfds.jpeg?q=70",
        "Description": "128 GB ROM |15.49 cm (6.1 inch) Super Retina XDR Display",
        "quantity": 1
      },
  
      {
        "id": 3,
        "name": "SAMSUNG Galaxy F54",
        "price": 30000,
        "color": "Stardust Silver",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/q/t/h/galaxy-f54-5g-sm-e546bzshins-samsung-original-imagq79f82pfyzvh.jpeg?q=70",
        "Description": "8 GB RAM | 256 GB ROM | Expandable Upto 1 TB",
        "quantity": 1
      },
      {
        "id": 4,
        "name": "SAMSUNG Galaxy F14",
        "price": 10000,
        "color": "OMG Black",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/j/b/r/galaxy-f14-5g-sm-e146bzkhins-samsung-original-imagnzdkvrt2sxrq.jpeg?q=70",
        "Description": "6 GB RAM | 128 GB ROM | Expandable Upto 1 TB",
        "quantity": 1
      },
  
      {
        "id": 6,
        "name": "vivo T2x 5G",
        "price": 30000,
        "color": "Marine Blue",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/k/u/n/-original-imagzjhwtfthcmzz.jpeg?q=70",
        "Description": "4 GB RAM | 128 GB ROM | Expandable Upto 1 TB",
        "quantity": 1
      },
      {
        "id": 5,
        "name": "OPPO A77s",
        "price": 20000,
        "color": "Starry Black",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/r/y/y/-original-imaggm3zwtuv7ggg.jpeg?q=70",
        "Description": "4 GB RAM | 128 GB ROM | 16.51 cm (6.5 inch) HD+ Display",
        "quantity": 1
      },
    
      {
        "id": 1,
        "name": " Multimedia Subwoofer Speaker ",
        "price": 500,
        "color": "Blue",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/speaker/laptop-desktop-speaker/g/u/b/multimedia-subwoofer-speaker-with-full-bass-sound-daily-needs-original-imagqapzwzhmkhgm.jpeg?q=70",
        "Description": "Daily Needs Shop Multimedia Subwoofer Speaker With Full Bass Sound 3 W Laptop/Desktop Speaker ",
        "quantity": 1
      },
      {
        "id": 2,
        "name": "Charger with Detachable Cable",
        "price": 400,
        "color": "White",
        "image": "https://rukminim1.flixcart.com/image/416/416/l48s9zk0/battery-charger/h/f/4/33w-vooc-dart-flash-dh561-with-type-c-cable-charging-adapter-original-imagf6j7zzdkzjwq.jpeg?q=70",
        "Description": "DART,FLASH DH561 with Type-C Cable Charging Adapter Travel Fast ",
        "quantity": 1
      },
      {
        "id": 3,
        "name": "Mi 3i 20000 mAh Power Bank",
        "price": 1000,
        "color": "Black",
        "image": "https://rukminim1.flixcart.com/image/416/416/kfcv6vk0/power-bank/r/f/5/power-bank-20000-plm18zm-mi-original-imafvtc7x9zgrzbz.jpeg?q=70",
        "Description": "Recharging time- 6.9 hours (with 18 W charger and USB cable)",
        "quantity": 1
      },
      {
        "id": 4,
        "name": "SWISS MILITARY B-YOND Smart Headphones ",
        "price": 1500,
        "color": "Black",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/smart-headphone/c/1/b/b-yond-swiss-military-original-imagnp6nabb34hzv.jpeg?q=70",
        "Description": "Wireless ,Time Tracking ,Supports Bluetooth Devices",
        "quantity": 1
      },
      {
        "id": 5,
        "name": "DSLR Camera",
        "price": 30000,
        "color": "Black",
        "image": "https://rukminim1.flixcart.com/image/416/416/jfbfde80/camera/n/r/n/canon-eos-eos-3000d-dslr-original-imaf3t5h9yuyc5zu.jpeg?q=70",
        "Description": "Canon EOS 3000D DSLR Camera 1 Camera Body, 18 - 55 mm Lens",
        "quantity": 1
      },
      {
        "id": 6,
        "name": "Smart Watch",
        "price": 1200,
        "color": "White",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/smartwatch/h/m/e/-original-imagkfm8fgvwjy8y.jpeg?q=70",
        "Description": " Curved Glass Display with 360 Health Training, 100+ Sports Modes Smartwatch ",
        "quantity": 1
      },
      {
        "id": 1,
        "name": "Kurta, Sharara and Dupatta Set",
        "price": 1200,
        "color": "White",
        "image": "https://rukminim1.flixcart.com/image/832/832/xif0q/ethnic-set/a/b/e/l-2108-aa-clr-01-black-scissor-original-imagjhehfpk32w4a.jpeg?q=70",
        "Description": "Women Kurta, Sharara and Dupatta Set Georgette",
        "quantity": 1
      },
  
      {
        "id": 2,
        "name": "Men Jeans",
        "price": 600,
        "color": "Blue",
        "image": "https://encrypted-tbn3.gstatic.com/shopping?q=tbn:ANd9GcQWgwa7n6LQK6b6ksW2xk7HLJSvFM_snlYrCMKQ7rOPPYC5CUy5bxHGAdlxdkbP_BQE3t7gXOW3vHFdztp1eA_ecwwr0nyz3zhaNCsb76A&usqp=CAE",
        "Description": "Metronaut By Flipkart Slim Men Dark Blue Jeans",
        "quantity": 1
      },
      {
        "id": 3,
        "name": "Saree",
        "price": 500,
        "color": "Red",
        "image": "https://rukminim1.flixcart.com/image/832/832/xif0q/sari/z/x/e/free-saree-with-georgette-fabric-sada-sobhagyavati-bhav-with-original-imagjnurkkccy8kg.jpeg?q=70",
        "Description": "Saree with georagette fabric sada sobhagyavati",
        "quantity": 1
      },
      {
        "id": 4,
        "name": "Women Printed Cotton Blend Flared Kurta  ",
        "price": 400,
        "color": "Beige",
        "image": "https://rukminim1.flixcart.com/image/832/832/xif0q/kurta/a/7/r/xs-kff00295-jenifer-peach-shrivani-original-imagmfzdczna3kek.jpeg?q=70",
        "Description": "Knee Length | Fabric Cotton",
        "quantity": 1
      },
      {
        "id": 5,
        "name": "Kurta, Pyjama & Dupatta Set",
        "price": 1000,
        "color": "Yellow",
        "image": "https://rukminim1.flixcart.com/image/832/832/xif0q/ethnic-set/b/7/9/l-y1002-klosia-original-imagmtffxfeqzgcd.jpeg?q=70",
        "Description": "Women Kurta, Pyjama & Dupatta Set Pure Cotton  ",
        "quantity": 1
      },
  
      {
        "id": 6,
        "name": "Cotton Blend Shirt",
        "price": 499,
        "color": "Sky Blue",
        "image": "https://rukminim1.flixcart.com/image/832/832/kwzap3k0/shirt/g/r/j/xl-lstr-skyblue-p-v-creations-original-imag9jgghrz3ynhf.jpeg?q=70",
        "Description": "BEING HUMAN  Men Solid Polo Neck Cotton Blend Green T-Shirt",
        "quantity": 1
      },
      {
        "id": 1,
        "name": "Box Bed",
        "price": 15000,
        "color": "Brown",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/bed/j/y/5/-original-imagpprf9wppvhuy.jpeg?q=70",
        "Description": "Flipkart Perfect Homes Waltz Engineered Wood King Box Bed",
        "quantity": 1
      },
      {
        "id": 2,
        "name": "Sofa",
        "price": 11000,
        "color": "Grey",
        "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/recliner/x/c/2/76-2-1-seater-grey-99-06-velvet-bls-recliner-grey-bharat-original-imagmwddn6yhcjnq.jpeg?q=70",
        "Description": "Lifestyle Austin Fabric Manual Recliner",
        "quantity": 1
      },
      {
        "id": 3,
        "name": "Painting",
        "price": 500,
        "color": "Brown",
        "image": "https://rukminim1.flixcart.com/image/416/416/jyrl4sw0/painting/3/x/g/pnf-5839-12x18frame-poster-n-frames-original-imafgxhnhzhuftcw.jpeg?q=70",
        "Description": "Poster N Frames UV Textured Decorative Art Print of Buddha ",
        "quantity": 1
      },
      {
        "id": 4,
        "name": "Darkening Door Curtain",
        "price": 500,
        "color": "Purle",
        "image": "https://rukminim1.flixcart.com/image/416/416/l0igvww0/curtain/i/c/u/cvc4re16-tre1pn16-door-eyelet-gratify-original-imagca7f3fpnhwpd.jpeg?q=70",
        "Description": "Vidhi-Tree-Punching-7-FT-2-Piece-Purple",
        "quantity": 1
      },
      {
        "id": 5,
        "name": "Analog Wall Watch",
        "price": 600,
        "color": "Brown With Glass, Standard",
        "image": "https://rukminim1.flixcart.com/image/416/416/kaijgy80/wall-clock/r/w/g/analoge-33-cm-x-33-cm-wall-clock-st-58745638-analog-ajanta-original-imafs2z2te2pchjz.jpeg?q=70",
        "Description": "Size: 33 cm x 33 cm ,Mechanism: wall mounted ",
        "quantity": 1
      },
      {
        "id": 6,
        "name": "Wall Lamp With Bulb",
        "price": 300,
        "color": "White & Gold ",
        "image": "https://rukminim1.flixcart.com/image/416/416/k2tc1ow0pkrrdj/wall-lamp/g/n/3/goj-22-114-white-shri-girraj-ji-original-imaeeyvs2ja338za.jpeg?q=70",
        "Description": "Material: [\"Glass\",\"Metal\"] ,Surface Mounted ,Bulb Used: [\"LED\"]",
        "quantity": 1
      }
    ]
  export default List ;