// import "../Images/photo.jpg";

const MobileList= [

  {
    "id": 1,
    "name": "APPLE iPhone 14",
    "price": 70000,
    "color": "Midnight",
    "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/9/e/e/-original-imaghx9q5rvcdghy.jpeg?q=70",
    "Description": "128 GB ROM | 15.49 cm (6.1 inch) Super Retina XDR Display" ,
    "quantity" : 1
  },
  {
    "id": 2,
    "name": "APPLE iPhone 14",
    "price": 70000,
    "color": "Purple",
    "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/b/u/f/-original-imaghxa5hvapbfds.jpeg?q=70",
    "Description": "128 GB ROM |15.49 cm (6.1 inch) Super Retina XDR Display" ,
    "quantity" : 1
  },

  {
    "id": 3,
    "name": "SAMSUNG Galaxy F54",
    "price": 30000,
    "color": "Stardust Silver",
    "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/q/t/h/galaxy-f54-5g-sm-e546bzshins-samsung-original-imagq79f82pfyzvh.jpeg?q=70",
    "Description": "8 GB RAM | 256 GB ROM | Expandable Upto 1 TB" ,
    "quantity" : 1
  },
  {
    "id": 4,
    "name": "SAMSUNG Galaxy F14",
    "price": 10000,
    "color": "OMG Black",
    "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/j/b/r/galaxy-f14-5g-sm-e146bzkhins-samsung-original-imagnzdkvrt2sxrq.jpeg?q=70",
    "Description": "6 GB RAM | 128 GB ROM | Expandable Upto 1 TB" ,
    "quantity" : 1
  },

  {
    "id": 6,
    "name": "vivo T2x 5G",
    "price": 30000,
    "color": "Marine Blue",
    "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/k/u/n/-original-imagzjhwtfthcmzz.jpeg?q=70",
    "Description": "4 GB RAM | 128 GB ROM | Expandable Upto 1 TB" ,
    "quantity" : 1
  },
  {
    "id": 5,
    "name": "OPPO A77s",
    "price": 20000,
    "color": "Starry Black",
    "image": "https://rukminim1.flixcart.com/image/416/416/xif0q/mobile/r/y/y/-original-imaggm3zwtuv7ggg.jpeg?q=70",
    "Description": "4 GB RAM | 128 GB ROM | 16.51 cm (6.5 inch) HD+ Display" ,
    "quantity" : 1
  }

];

 


export default MobileList;
