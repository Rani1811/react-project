const FashionList = [
  {
    id: 1,
    name:'Kurta, Sharara and Dupatta Set',
    price: 1200,
    color:"White",
    image:'https://rukminim1.flixcart.com/image/832/832/xif0q/ethnic-set/a/b/e/l-2108-aa-clr-01-black-scissor-original-imagjhehfpk32w4a.jpeg?q=70',
    Description:'Women Kurta, Sharara and Dupatta Set Georgette',
   },
    
    {
     id:2 ,
     name:'Men Jeans',
     price: 600,
     color:"Blue",
     image:'https://encrypted-tbn3.gstatic.com/shopping?q=tbn:ANd9GcQWgwa7n6LQK6b6ksW2xk7HLJSvFM_snlYrCMKQ7rOPPYC5CUy5bxHGAdlxdkbP_BQE3t7gXOW3vHFdztp1eA_ecwwr0nyz3zhaNCsb76A&usqp=CAE',
     Description:'Metronaut By Flipkart Slim Men Dark Blue Jeans',
    },
    {
        id:3 ,
        name:'Saree',
        price: 500,
        color:"Red",
        image:'https://rukminim1.flixcart.com/image/832/832/xif0q/sari/z/x/e/free-saree-with-georgette-fabric-sada-sobhagyavati-bhav-with-original-imagjnurkkccy8kg.jpeg?q=70',
        Description:'Saree with georagette fabric sada sobhagyavati',
       },
       {
        id:4 ,
        name:'Women Printed Cotton Blend Flared Kurta  ',
        price:400 ,
        color:"Beige",
        image:'https://rukminim1.flixcart.com/image/832/832/xif0q/kurta/a/7/r/xs-kff00295-jenifer-peach-shrivani-original-imagmfzdczna3kek.jpeg?q=70',
        Description:'Knee Length | Fabric Cotton',
       },
       {
        id:5 ,
        name:'Kurta, Pyjama & Dupatta Set',
        price: 1000,
        color:"Yellow",
        image:'https://rukminim1.flixcart.com/image/832/832/xif0q/ethnic-set/b/7/9/l-y1002-klosia-original-imagmtffxfeqzgcd.jpeg?q=70',
        Description:'Women Kurta, Pyjama & Dupatta Set Pure Cotton  ',
       },
      
       {
        id: 6,
        name: 'Cotton Blend Shirt',
        price: 499 ,
        color:"Sky Blue",
        image: 'https://rukminim1.flixcart.com/image/832/832/kwzap3k0/shirt/g/r/j/xl-lstr-skyblue-p-v-creations-original-imag9jgghrz3ynhf.jpeg?q=70',
        Description:"BEING HUMAN  Men Solid Polo Neck Cotton Blend Green T-Shirt",
      },


] 

export default FashionList ;