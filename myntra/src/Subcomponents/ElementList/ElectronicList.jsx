const ElectronicList =[
    {
        id: 1,
        name:' Multimedia Subwoofer Speaker ',
        price:  500,
        color:"Blue",
        image: 'https://rukminim1.flixcart.com/image/416/416/xif0q/speaker/laptop-desktop-speaker/g/u/b/multimedia-subwoofer-speaker-with-full-bass-sound-daily-needs-original-imagqapzwzhmkhgm.jpeg?q=70',
        Description:"Daily Needs Shop Multimedia Subwoofer Speaker With Full Bass Sound 3 W Laptop/Desktop Speaker ",
      },
      {
        id: 2,
        name: 'Charger with Detachable Cable',
        price: 400 ,
        color:"White",
        image: 'https://rukminim1.flixcart.com/image/416/416/l48s9zk0/battery-charger/h/f/4/33w-vooc-dart-flash-dh561-with-type-c-cable-charging-adapter-original-imagf6j7zzdkzjwq.jpeg?q=70',
        Description:"DART,FLASH DH561 with Type-C Cable Charging Adapter Travel Fast ",
      },
      {
        id: 3,
        name: 'Mi 3i 20000 mAh Power Bank',
        price: 1000 ,
        color:"Black",
        image: 'https://rukminim1.flixcart.com/image/416/416/kfcv6vk0/power-bank/r/f/5/power-bank-20000-plm18zm-mi-original-imafvtc7x9zgrzbz.jpeg?q=70',
        Description:"Recharging time- 6.9 hours (with 18 W charger and USB cable)",
      },
      {
        id: 4,
        name: 'SWISS MILITARY B-YOND Smart Headphones ',
        price: 1500,
        color:"Black",
        image: 'https://rukminim1.flixcart.com/image/416/416/xif0q/smart-headphone/c/1/b/b-yond-swiss-military-original-imagnp6nabb34hzv.jpeg?q=70',
        Description:"Wireless ,Time Tracking ,Supports Bluetooth Devices",
      },
      {
        id: 5,
        name: 'DSLR Camera',
        price:  30000,
        color:"Black",
        image: 'https://rukminim1.flixcart.com/image/416/416/jfbfde80/camera/n/r/n/canon-eos-eos-3000d-dslr-original-imaf3t5h9yuyc5zu.jpeg?q=70',
        Description:"Canon EOS 3000D DSLR Camera 1 Camera Body, 18 - 55 mm Lens  ",
      },
      {
        id: 6,
        name: 'Smart Watch',
        price:  1200,
        color:"White",
        image: 'https://rukminim1.flixcart.com/image/416/416/xif0q/smartwatch/h/m/e/-original-imagkfm8fgvwjy8y.jpeg?q=70',
        Description:" Curved Glass Display with 360 Health Training, 100+ Sports Modes Smartwatch ",
      },

]

export default ElectronicList ;