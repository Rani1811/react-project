
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { addToCart } from "../Feature/MobileSlice";

import "../ComponentsCss/Card.css";


export default function App() {

    const mobiles = useSelector((state) => state.MobileCart.items);

    const dispatch = useDispatch();
   
 
  return (
    <div style={{display:"flex", flexWrap: "wrap", justifyContent: "space-between"}}>
      {mobiles.map((mobile) => (
        <div key={mobile.id}>
          <div className="card-bodys" key={mobile.id} style={{height: "520px" }}>
            <img
              src={mobile.image}
              style={{ width: 180, height: 250 }}
              alt={mobile.name}
            />
            <h6 className="card-datas" style={{ fontWeight: "bold" }}>
              {mobile.name}
            </h6>
            <h6 className="card-datas">
              ₹{mobile.price} , {mobile.color}
            </h6>
            <h6 className="card-datas">{mobile.Description}</h6>
              <button className="addCarts" onClick={ () => dispatch(addToCart(mobile), alert("Product Add to cart"))}>Add to Cart</button>
          </div>
        </div>
      ))}
    </div>
  );
};















// const mapStateToProps = (state) => {
//   return {
//     mobiles: state.mobiles,
//     loading: state.loading,
//     error: state.error,
    
//   };
// };

// const mapDispatchToProps = {
//   fetchmobile,

// };


// export default connect(mapStateToProps, mapDispatchToProps)(mobileList);

// import React, { useEffect } from 'react';
// import { connect } from 'react-redux';
// import { fetchMobile } from '../Action/action';
// // import '../ComponentsCss/Card.css';

// const MobileList = ({ mobiles, loading, error, fetchMobile }) => {
//   useEffect(() => {
//     fetchMobile();
//   }, [fetchMobile]);

//   if (loading) {
//     return <p>Loading...</p>;
//   }

//   if (error) {
//     return <p>Error: {error}</p>;
//   }

//   return (
//     <>
//       {mobiles.map(mobile => (
//         <div key={mobile.id}>
//           <div className="card-body" key={mobile.id}>
//             <img src={mobile.image} style={{ width: 180, height: 250 }} alt={mobile.name} />
//             <h6 className="card-data" style={{ fontWeight: "bold" }}>{mobile.name}</h6>
//             <h6 className="card-data">₹{mobile.price}, {mobile.color}</h6>
//             <h6 className="card-data">{mobile.Description}</h6>
//           </div>
//         </div>
//       ))}
//     </>
//   );
// };

// const mapStateToProps = (state) => {
//   return {
//     mobiles: state.mobiles,
//     loading: state.loading,
//     error: state.error
//   };
// };

// const mapDispatchToProps = {
//   fetchMobile
// };

// export default connect(mapStateToProps, mapDispatchToProps)(MobileList);