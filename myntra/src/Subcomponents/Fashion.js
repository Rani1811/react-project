// import React, { useEffect } from 'react';
// import { connect } from 'react-redux';
// import { fetchFashion } from '../Action/action';
// // import '../ComponentsCss/Card.css';

// const FashionList = ({fashions, fetchFashion }) => {
//   useEffect(() => {
//     fetchFashion();
//   }, [fetchFashion]);


//   return (
//     <div>
//       {fashions.map(fashion => (
//         <div key={fashion.id}>
//           <div className="card-body" key={fashion.id}>
//           <img src={fashion.image} style={{width:180 , height:250}} alt={fashion.name} />
//            <h6 className="card-data" style={{fontWeight:"bold"}}>{fashion.name}</h6>
//            <h6 className="card-data">₹{fashion.price} , {fashion.color}</h6>
//            <h6 className="card-data">{fashion.Description}</h6>

//         </div>
//         </div>
//       ))}
//     </div>
//   );
// };

// const mapStateToProps = (state) => {
//   return {
//     fashions: state.fashions,
//     loading: state.loading,
//     error: state.error
//   };
// };

// const mapDispatchToProps = {
//   fetchFashion
// };

// export default connect(mapStateToProps, mapDispatchToProps)(FashionList);




import React from "react";

import { useSelector, useDispatch } from "react-redux";
import { addToCart } from "../Feature/FashionSlice";

import "../ComponentsCss/Card.css";


export default function App() {

    const appliances = useSelector((state) => state.FashionCart.items);

    const dispatch = useDispatch();

  return (
    <div style={{display:"flex", flexWrap: "wrap", justifyContent: "space-between"}}>
      {appliances.map((appliance) => (
        <div key={appliance.id}>
          <div className="card-bodys" key={appliance.id} style={{height: "520px" }}>
            <img
              src={appliance.image}
              style={{ width: 180, height: 250 }}
              alt={appliance.name}
            />
            <h6 className="card-datas" style={{ fontWeight: "bold" }}>
              {appliance.name}
            </h6>
            <h6 className="card-datas">
              ₹{appliance.price} , {appliance.color}
            </h6>
            <h6 className="card-datas">{appliance.Description}</h6>
              <button className="addCarts" onClick={ () => dispatch(addToCart(appliance), alert("Product Add to cart"))}>Add to Cart</button>
          </div>
        </div>
      ))}
    </div>
  );
};

