

import * as React from "react";
import { useState } from "react";
import "../ComponentsCss/Login.css";
import  UserList from "../Components/userlist";
import { NavLink } from "react-router-dom";

import { useNavigate } from 'react-router-dom';

const ReactiveForm = () => {
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
 

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({
      ...formData,
      [name]: value,
    }));
  };




  const handleSubmit= (e) => {
   
    
    // Check if the user input matches the data in the userlist
    const matchingUser = UserList.findIndex((item) => item.email === formData.email && item.password === formData.password);    
     console.log(matchingUser);
     e.preventDefault();
    if (matchingUser) {
      // alert("Login successful");
      navigate('/');

  
  
      // Perform any additional actions for successful login
    } else {
      alert("Invalid email or password");
      // Handle invalid login attempt
    }

  };

  return (
    <>
      <div className="regis">
        <h2>Login</h2>
      </div>
      <form className="form" onSubmit={handleSubmit}>
        <label>Email:</label>
        <input
          type="text"
          name="email"
          placeholder="Enter email.."
          required
          id="email"
          value={formData.email}
          // onChange={(e) =>setemail(e.target.value)}
          onChange={handleChange}
        />

        <label>Password:</label>
        <input
          type="text"
          name="password"
          placeholder="Enter password.."
          required
          id="password"
          value={formData.password}
          // onChange={(e) =>setpassword(e.target.value)} onClick={login}
          onChange={handleChange}
        />

        <button   type="submit" id="Submit" name="submit">
          Login
        </button>
        <p style={{color:"black"}}>Don't have an account? <NavLink to="/RegistrationForm">Register</NavLink></p>
      </form>
    </>
  );
};

export default ReactiveForm;









//  async function login (e) {
    
//   let items ={email ,password} ;
//   let result = await fetch("http://localhost:3000/userlist" ,{
//      method:'POST',
//      headers:{
//       "content-type":"application/json",
//       "Accept":"application/json",
//      },
//      body:JSON.stringify(items)

//   }) ;
//   result = await result.json() ;
//   // localStorage.setItem("userlist",JSON.stringify(result));
//   navigate('/');
  
// console.log(result);
// e.preventDefault();

// }


// import * as React from "react";
// import { useState } from "react";
// import "../ComponentsCss/Login.css";


// import { updateUserList } from "../Components/userlist"; // Import the updateUserList function from userlist.jsx

// const ReactiveForm = () => {
//   const [formData, setFormData] = useState({
   
//     mobile: "",
//     email: "",
   
//     password: "",
//   });

//   const handleChange = (e) => {
//     const { name, value } = e.target;
//     setFormData((formData) => ({
//       ...formData,
//       [name]: value,
//     }));
//   };

//   const handleSubmit = (e) => {
//     // e.preventDefault();
    
//     // Call the updateUserList function from userlist.jsx and pass the formData
     


//     updateUserList(formData);
//     alert("Registration successful");
//   };

//   return (
//     <>
//       {/* <Navbar /> */}
//       <div className="regis">
//         <h2>Login</h2>
//       </div>
//       <form className="form" onSubmit={handleSubmit}>
        

//         <label>Email:</label>
//         <input
//           type="text"
//           name="email"
//           placeholder="Enter email.."
//           required
//           id="email"
//           value={formData.email}
//           onChange={handleChange}
//         />

//         <label>Mobile:</label>
//         <input
//           type="text"
//           name="mobile"
//           placeholder="Enter mobile no.."
//           required
//           id="mobile"
//           value={formData.mobile}
//           onChange={handleChange}
//         />

//         <label>Password:</label>
//         <input
//           type="text"
//           name="password"
//           placeholder="Enter password.."
//           required
//           id="password"
//           value={formData.password}
//           onChange={handleChange}
//         />

//         <button type="submit" id="Submit" name="submit" >Login</button>
//       </form>
//     </>
//   );
// };



// export default ReactiveForm;
