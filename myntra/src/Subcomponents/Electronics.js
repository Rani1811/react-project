// import React, { useEffect } from 'react';
// import { connect } from 'react-redux';
// import { fetchelectronic } from '../Action/action';
// // import '../ComponentsCss/Card.css';

// const Electroniclist = ({electronics, fetchelectronic }) => {
//   useEffect(() => {
//     fetchelectronic();
//   }, [fetchelectronic]);


//   return (
//     <div>
//       {electronics.map(electronic => (
//         <div key={electronic.id}>
//           <div className="card-body" key={electronic.id}>
//           <img src={electronic.image} style={{width:180 , height:250}} alt={electronic.name} />
//            <h6 className="card-data" style={{fontWeight:"bold"}}>{electronic.name}</h6>
//            <h6 className="card-data">₹{electronic.price} , {electronic.color}</h6>
//            <h6 className="card-data">{electronic.Description}</h6>

//         </div>
//         </div>
//       ))}
//     </div>
//   );
// };

// const mapStateToProps = (state) => {
//   return {
//     electronics: state.electronics,
//     loading: state.loading,
//     error: state.error
//   };
// };

// const mapDispatchToProps = {
//   fetchelectronic
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Electroniclist);

import React from "react";

import { useSelector, useDispatch } from "react-redux";
import { addToCart } from "../Feature/ElectronicsSlice";

import "../ComponentsCss/Card.css";


export default function App() {

    const appliances = useSelector((state) => state.ElectronicsCart.items);

    const dispatch = useDispatch();

  return (
    <div style={{display:"flex", flexWrap: "wrap", justifyContent: "space-between"}}>
      {appliances.map((appliance) => (
        <div key={appliance.id}>
          <div className="card-bodys" key={appliance.id} style={{height: "520px" }}>
            <img
              src={appliance.image}
              style={{ width: 180, height: 250 }}
              alt={appliance.name}
            />
            <h6 className="card-datas" style={{ fontWeight: "bold" }}>
              {appliance.name}
            </h6>
            <h6 className="card-datas">
              ₹{appliance.price} , {appliance.color}
            </h6>
            <h6 className="card-datas">{appliance.Description}</h6>
              <button className="addCarts" onClick={ () => dispatch(addToCart(appliance), alert("Product Add to cart"))}>Add to Cart</button>
          </div>
        </div>
      ))}
    </div>
  );
};

