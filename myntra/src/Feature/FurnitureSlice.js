import { createSlice } from "@reduxjs/toolkit";
import FurnitureList from "../Subcomponents/ElementList/FurnitureList";

const initialState = {
  cart: [],
  items:FurnitureList,
  totalQuantity: 0,
  totalPrice: 0,
};


export const FashionSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action) => {
      let find = state.cart.findIndex((item) => item.name === action.payload.name);
      if (find >= 0) {
        state.cart[find].quantity += 1;
      } else {
        state.cart.push(action.payload);
      }
    },

    getCartTotal: (state) => {
        let { totalQuantity, totalPrice } = state.cart.reduce (
            (cartTotal, cartItem) => {
                const { price, quantity } = cartItem;
                const itemTotal = price * quantity;
                cartTotal.totalPrice += itemTotal;
                cartTotal.totalQuantity += quantity;
                return cartTotal; 
            },
            {
                totalPrice: 0,
                totalQuantity: 0,
            }
        );
        state.totalPrice = parseInt(totalPrice.toFixed(2));
        state.totalQuantity = totalQuantity;
    },
     
    removeItem: (state,action) => {
         state.cart = state.cart.filter((item) => item.name !== action.payload);
    },

    increaseItemQuantity: (state, action) => {
        state.cart = state.cart.map((item) => {
            if(item.name === action.payload){
                return {...item, quantity : item.quantity+1};
            }
            return item;
        });
    },

    decreaseItemQuantity: (state, action) => {
        state.cart = state.cart.map((item) => {
            if(item.name === action.payload){
                return {...item, quantity : item.quantity-1};
            }
            return item;
        });
    },

  },
});

export const { addToCart, getCartTotal, removeItem, increaseItemQuantity, decreaseItemQuantity } = FashionSlice.actions;

export default FashionSlice.reducer;
