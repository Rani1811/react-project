import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "../Feature/ApplianceSlice";
import postReducer from "../Feature/MobileSlice";
import fashionReducer from "../Feature/FashionSlice" ;
import electronicReducer from "../Feature/ElectronicsSlice"
import funitureReducer from "../Feature/FurnitureSlice"

export  const store = configureStore({
    reducer :{
        ApplianceCart : cartReducer,
        MobileCart : postReducer ,
        FashionCart :fashionReducer ,
        ElectronicsCart: electronicReducer ,
        FurnitureCart : funitureReducer ,
    },

    
}) ;
