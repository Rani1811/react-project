import React from "react";
import { render, screen ,fireEvent} from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import RegistrationForm from "./RegistrationForm";

test("Registration form renders with correct heading", () => {
  render(
    <Router>
      <RegistrationForm />
    </Router>
  );

  const headingElement = screen.getByRole("heading", { name: /Registration/i });
  expect(headingElement).toBeTruthy();
});

    
 test('Elements in registration form ' , () =>{
    render(
        <Router>
          <RegistrationForm />
        </Router>
      );

      const name =screen.getByText(/Name/i);
      expect(name).toBeTruthy();

      const Email =screen.getByText(/Email/i);
      expect(Email).toBeTruthy();

      const mobile=screen.getByText(/Mobile/i);
      expect(mobile).toBeTruthy();

     

      const password =screen.getByText(/Password/i);
      expect(password).toBeTruthy();


 })

 test('button' ,() => {
    render(
        <Router>
          <RegistrationForm />
        </Router>
      );

    //   const buttonlist =screen.getByRole("button");
    //   console.log(buttonlist);
    //   expect(buttonlist).toHaveLength(1);
    const submitButton = screen.getByRole('button', { name: /Submit/i });
    fireEvent.click(submitButton);

 })