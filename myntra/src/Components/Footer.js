import React from "react";
import "../ComponentsCss/Footer.css";
function Footer() {
  return (
    <>
      <>
        {/* Footer */}
        <footer className="text-center text-lg-start bg-dark text-muted">
          {/* Section: Social media */}
          <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
            {/* Left */}
            <div className="me-5 d-none d-lg-block">
              <span>Get connected with us on social networks:</span>
            </div>
            {/* Left */}
            {/* Right */}
            <div>
              <a href="" className="me-4 text-reset">
                <i className="fab fa-facebook-f" />
              </a>
              <a href="" className="me-4 text-reset">
                <i className="fab fa-twitter" />
              </a>
              <a href="" className="me-4 text-reset">
                <i className="fab fa-google" />
              </a>
              <a href="" className="me-4 text-reset">
                <i className="fab fa-instagram" />
              </a>
              <a href="" className="me-4 text-reset">
                <i className="fab fa-linkedin" />
              </a>
              <a href="" className="me-4 text-reset">
                <i className="fab fa-github" />
              </a>
            </div>
            {/* Right */}
          </section>
          {/* Section: Social media */}
          {/* Section: Links  */}
          <section className="">
            <div className="container text-center text-md-start mt-5">
              {/* Grid row */}
              <div className="row mt-3">
                {/* Grid column */}
                {/* <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
            Content
            <h6 className="text-uppercase fw-bold mb-4">
              <i className="fas fa-gem me-3" />
              E-Cart
            </h6>
            <p>
              Here you can use rows and columns to organize your footer content.
              Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            </p>
          </div> */}

                {/* Grid column */}
                {/* Grid column */}

                <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                  {/* Links */}
                  <h6 className="text-uppercase fw-bold mb-4">About </h6>
                  <p>
                    <a href="#!" className="text-reset">
                      Contact us
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      About us
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      Careers
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      ShopCart Stories
                    </a>
                  </p>
                </div>

                <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                  {/* Links */}
                  <h6 className="text-uppercase fw-bold mb-4">Help</h6>
                  <p>
                    <a href="#!" className="text-reset">
                      Payment
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      Shipping
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      Cancellation
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      FAQ
                    </a>
                  </p>
                </div>
                {/* Grid column */}
                {/* Grid column */}
                <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                  {/* Links */}
                  <h6 className="text-uppercase fw-bold mb-4">
                    Consumer Policy
                  </h6>
                  <p>
                    <a href="#!" className="text-reset">
                      Return Policy
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      Term of use
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      Security
                    </a>
                  </p>
                  <p>
                    <a href="#!" className="text-reset">
                      Privacy
                    </a>
                  </p>
                </div>

                {/* Grid column */}
                {/* Grid column */}
                <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                  {/* Links */}
                  <h6 className="text-uppercase fw-bold mb-4">Mail us</h6>
                  <p>
                ShopCart Internet Private Limited, Buildings Alyssa, Begonia &
                    Clove Embassy Tech Village, Outer Ring Road,
                    Devarabeesanahalli Village, Bengaluru, 560103, Karnataka,
                    India
                  </p>
                </div>

                <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                  {/* Content */}
                  <h6 className="text-uppercase fw-bold mb-4">
                    <i className="fas fa-gem me-3" />
                    Register office address
                  </h6>
                  <p>
                    ShopCart Internet Private Limited, Buildings Alyssa, Begonia &
                    Clove Embassy Tech Village, Outer Ring Road,
                    Devarabeesanahalli Village, Bengaluru, 560103, Karnataka,
                    India CIN : U51109KA2012PTC066107 Telephone: 044-45614700
                  </p>
                </div>

                {/* Grid column */}
              </div>
              {/* Grid row */}
            </div>
          </section>
          {/* Section: Links  */}
          {/* Copyright */}
          <div
            className="text-center p-4"
            style={{ backgroundColor: "rgba(0, 0, 0, 0.05)" }}
          >
            © 2021 Copyright:
            <a className="text-reset fw-bold" href="https://mdbootstrap.com/">
              ekart.com
            </a>
          </div>
          {/* Copyright */}
        </footer>
        {/* Footer */}
      </>
    </>
  );
}
export default Footer;
