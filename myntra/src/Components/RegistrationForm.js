import * as React from "react";
import { useState } from "react";
import "../ComponentsCss/Register.css";

// import Navbar from "./Navbar";
import { updateUserList } from "./userlist"; // Import the updateUserList function from userlist.jsx

const ReactiveForm = () => {
  const [formData, setFormData] = useState({ //initializes the components
    firstName: "",
    mobile: "",
    email: "",
   
    password: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((formData) => ({ //update the input
      ...formData,
      [name]: value,
    }));
  };

  const handleSubmit = (e) => { //called for the form submission
    e.preventDefault();
    console.log(formData);
    updateUserList(formData);
    alert("Registration successful");
  };

  return (
    <>
      <div className="regis">
        <h2>Registration</h2>
      </div>
      <form className="form" onSubmit={handleSubmit}>
        <label>Name:</label>
        <input
          type="text"
          name="firstName"
          placeholder="Enter first name.."
          required
          id="fname"
          value={formData.firstName}
          onChange={handleChange}
        />

        <label>Email:</label>
        <input
          type="text"
          name="email"
          placeholder="Enter email.."
          required
          id="email"
          value={formData.email}
          onChange={handleChange}
        />

        <label>Mobile:</label>
        <input
          type="text"
          name="mobile"
          placeholder="Enter mobile no.."
          required
          id="mobile"
          value={formData.mobile}
          onChange={handleChange}
        />


        <label>Password:</label>
        <input
          type="text"
          name="password"
          placeholder="Enter password.."
          required
          id="password"
          value={formData.password}
          onChange={handleChange}
        />

        <input type="submit" id="Submit" name="submit" />
      </form>
    </>
  );
};

// export default function regis() {
//   return (
//     <>
//       <Navbar />
//       <div className="regis">
//         <section>

//           <ul>
//             <ReactiveForm />
//           </ul>
//         </section>
//       </div>
//     </>
//   );
// }

export default ReactiveForm;
