import React from "react";
import "../ComponentsCss/Home.css";
import { NavLink } from "react-router-dom";
import Mobiles from "../Subcomponents/Mobiles";
import Electronics from "../Subcomponents/Electronics";
import Appliance from "../Subcomponents/Appliance";
import Fashion from "../Subcomponents/Fashion";
import HomeFurniture from "../Subcomponents/Home&Furniture";
import { useNavigate } from 'react-router-dom';
function Home() {
  const navigate = useNavigate();

  const handleclick =() => {
   navigate('/Mobiles')    
  }
        
  return (
    <>
  
    
        <div className="bg">
          <div className="text">
            <h2 className='it'>APPLE iPhone 14</h2><br></br>
            <p> Buy now Just<b style={{ color: 'RED' }} >  ₹ 70,000</b></p>
            <button type="button"  onClick={handleclick} style={{width:200 ,height:50}}>SHOP NOW</button>

          </div>
        </div>
    
      
       <h1><NavLink to="/Mobiles">Best of Mobiles</NavLink></h1> 
      <Mobiles />
      <h1> <NavLink to="/Appliance">Best of Appliance</NavLink></h1> 
      <Appliance />
      <h1><NavLink to="/Electronics">Best of Electronics</NavLink></h1> 
      <Electronics />
      <h1> <NavLink to="/Fashion">Best of Fashion </NavLink></h1> 
      <Fashion />
      <h1> <NavLink to="/Home&Furniture">Best of HomeFurniture </NavLink></h1> 
      <HomeFurniture />
       

          
    </>
  );
}

export default Home;
