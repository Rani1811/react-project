import React, { useState } from 'react';
import ElementList from '../Subcomponents/ElementList/Elementlist';
// import Navbar from './Navbar';
import "../ComponentsCss/Card.css";

function SearchBar() {
  const [searchitem, setSearchitem] = useState('');
  const [filteredUsers, setFilteredUsers] = useState(ElementList);

  const handleInputChange = (event) => {
    setSearchitem(event.target.value); //update the searchitem state 
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    filterUsers(searchitem);
  };

  const filterUsers = (searchTerm) => {
    const filtered = ElementList.filter((item) =>
      item.name.toLowerCase().includes(searchTerm.toLowerCase())

    );
    setFilteredUsers(filtered);
  };





  return (
    <>
    {/* <Navbar/> */}
    <div className='react'>
      <form onSubmit={handleFormSubmit}>
        <input
          type="text"
          placeholder="Search by username..."
          value={searchitem}
          onChange={handleInputChange}
        />
        <button type="submit">Search</button>
      </form>
      <div className="item-list">
        {filteredUsers.length === 0 ? (
          <p>No matching users found.</p>
        ) : (
          <ul>
            {filteredUsers.map((appliance) => (
             
                <div key={appliance.name}>
          <div className="card-bodys" key={appliance.name} style={{height: "520px" }}>
            <img
              src={appliance.image}
              style={{ width: 180, height: 250 }}
              alt={appliance.name}
            />
            <h6 className="card-datas" style={{ fontWeight: "bold" }}>
              {appliance.name}
            </h6>
            <h6 className="card-datas">
              ₹{appliance.price} , {appliance.color}
            </h6>
            <h6 className="card-datas">{appliance.Description}</h6>
              {/* <button className="addCarts" onClick={ () => dispatch(addToCart(appliance))}>Add to Cart</button> */}
          </div>
        </div>
                
            ))} 
          </ul>
        )}
      </div>
    </div>
    </>
  );
}

export default SearchBar;






























// function search() {
//     // Move the JavaScript code inside a function that is executed after the DOM has loaded
//     document.addEventListener("DOMContentLoaded", function() {
//       var input, filter, cards, cardContainer, title, i;
//       input = document.getElementById("myFilter");
//       filter = input.value.toUpperCase();
//       cardContainer = document.getElementById("myProducts");
//       cards = cardContainer.getElementsByClassName("container");
//       for (i = 0; i < cards.length; i++) {
//         title = cards[i].querySelector("card-title");
//         if (title.innerText.toUpperCase().indexOf(filter) > -1) {
//           cards[i].style.display = "";
//         } else {
//           cards[i].style.display = input;
//         }
//       }
//     });
  
//     return (
//       <div className="srcinput">
//         <input type="text" id="myFilter" class="form-control" placeholder="Search for username..." onkeyup="search()" />
//       </div>
//     );
//   }
  
//   export default search;
  