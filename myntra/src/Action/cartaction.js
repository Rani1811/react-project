export const addToCart = (appliances) => {
    return {
      type: 'ADD_TO_CART',
      payload: appliances
    };
  };
  
  export const removeFromCart = (productId) => {
    return {
      type: 'REMOVE_FROM_CART',
      payload: productId
    };
  };
  