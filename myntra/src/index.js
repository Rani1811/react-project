import React from "react";
import ReactDOM from "react-dom/client";
import {store} from './Store/store';
import { BrowserRouter } from "react-router-dom";
import 'mdb-react-ui-kit/dist/css/mdb.min.css';
import "@fortawesome/fontawesome-free/css/all.min.css";
import App from "./App";
import { Provider } from "react-redux";
import Footer from "./Components/Footer";




const root = ReactDOM.createRoot(document.getElementById("root"));


root.render(
  <Provider store={store}>
  <BrowserRouter>
    <React.StrictMode>
      <App />
      <Footer />
    </React.StrictMode>
  </BrowserRouter>
  </Provider>
);
